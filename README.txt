Esta página web está diseñada a mi medida para mostrar de forma sencilla y sin florituras las redes sociales que uso, las contribuciones que he hecho a lo largo del tiempo en proyectos de código abierto y para facilitar una manera de conocerme mejor. No hay anuncios, rastreadores ni telemetría en esta página web. ¡Ni siquiera se usan cookies! Además, tiene una puntuación perfecta en la prueba de rendimiento de Lighthouse.
Espero que os guste.

